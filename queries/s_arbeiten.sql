SELECT KandNr as Kandidatennummer, person.name AS KandidatName, person.vorname AS KandidatVorname, aufgabenstellung.voll AS Aufgabenstellung, expertUser.Name AS ExpertenName, spezialgebiet.bez AS Spezialgebiet FROM zuteilung
LEFT JOIN experte ON zuteilung.pid = experte.pid
LEFT JOIN person AS expertUser ON experte.pid = expertUser.pid
LEFT JOIN aufgabenstellung ON zuteilung.aid = aufgabenstellung.aid
LEFT JOIN kandidat ON aufgabenstellung.aid = kandidat.pid
LEFT JOIN person ON kandidat.pid = person.pid
LEFT JOIN spezialgebiet ON aufgabenstellung.spezialgebiet = spezialgebiet.sid
ORDER BY Kandidatennummer;
