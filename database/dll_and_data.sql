-- MySQL dump 10.13  Distrib 5.5.27, for Win32 (x86)
--
-- Host: localhost    Database: m141ex
-- ------------------------------------------------------
-- Server version	5.5.27

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


DROP DATABASE IF EXISTS `school`;
CREATE DATABASE `school`;
USE `school`;

--
-- Table structure for table `aufgabenstellung`
--

DROP TABLE IF EXISTS `aufgabenstellung`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `aufgabenstellung` (
  `aid` int(11) NOT NULL DEFAULT '0',
  `kurz` varchar(255) DEFAULT NULL,
  `titel` varchar(50) NOT NULL,
  `voll` varchar(50) DEFAULT NULL,
  `spezialgebiet` int(11) DEFAULT '0',
  PRIMARY KEY (`aid`),
  UNIQUE KEY `kandidataufgabenstellung` (`aid`),
  KEY `aid` (`aid`),
  KEY `spezialgebietaufgabenstellung` (`spezialgebiet`),
  CONSTRAINT `FK_spezialgebiet_aufgabenstellung` FOREIGN KEY (`spezialgebiet`) REFERENCES `spezialgebiet` (`sid`),
  CONSTRAINT `FK_kandidat_aufgabenstellung` FOREIGN KEY (`aid`) REFERENCES `kandidat` (`pid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `aufgabenstellung`
--

LOCK TABLES `aufgabenstellung` WRITE;
/*!40000 ALTER TABLE `aufgabenstellung` DISABLE KEYS */;
INSERT INTO `aufgabenstellung` VALUES (4,'','Ablösung Layer 2 Netz','',1),(5,'','Systemüberwachung Intranet','',3),(7,'','Webbasierte Infobox','',2);
/*!40000 ALTER TABLE `aufgabenstellung` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `experte`
--

DROP TABLE IF EXISTS `experte`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `experte` (
  `pid` int(11) NOT NULL DEFAULT '0',
  `anrede` varchar(20) DEFAULT NULL,
  `funktion` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`pid`),
  UNIQUE KEY `personexperte` (`pid`),
  KEY `funktionexperte` (`funktion`),
  KEY `pid` (`pid`),
  CONSTRAINT `FK_person_experte` FOREIGN KEY (`pid`) REFERENCES `person` (`pid`),
  CONSTRAINT `FK_funktion_experte` FOREIGN KEY (`funktion`) REFERENCES `funktion` (`tid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `experte`
--

LOCK TABLES `experte` WRITE;
/*!40000 ALTER TABLE `experte` DISABLE KEYS */;
INSERT INTO `experte` VALUES (6,'Dr.',2),(7,'Herr',1),(8,'Herr',1);
/*!40000 ALTER TABLE `experte` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `firma`
--

DROP TABLE IF EXISTS `firma`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `firma` (
  `fid` int(11) NOT NULL DEFAULT '0',
  `name` varchar(50) DEFAULT NULL,
  `adr_strasse` varchar(50) DEFAULT NULL,
  `adr_plz` varchar(6) DEFAULT NULL,
  `adr_ort` varchar(50) DEFAULT NULL,
  `tel_zentrale` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`fid`),
  KEY `fid` (`fid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `firma`
--

LOCK TABLES `firma` WRITE;
/*!40000 ALTER TABLE `firma` DISABLE KEYS */;
INSERT INTO `firma` VALUES (100,'ETA','Schild-Ruststrasse','2540','Grenchen','032 655 71 11'),(101,'Scintilla','Luterbachstrasse 10','4528','Zuchwil','032 686 31 11'),(102,'BDO Visura','Biberiststr. 16','4500','Solothurn','032 624 67 63'),(103,'Bürgerspital','Schöngrünstrasse 38','4500','Solothurn','032 627 31 21');
/*!40000 ALTER TABLE `firma` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `funktion`
--

DROP TABLE IF EXISTS `funktion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `funktion` (
  `tid` int(11) NOT NULL AUTO_INCREMENT,
  `funktion` varchar(50) NOT NULL,
  PRIMARY KEY (`tid`),
  KEY `funktionfunktion` (`funktion`),
  KEY `tid` (`tid`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `funktion`
--

LOCK TABLES `funktion` WRITE;
/*!40000 ALTER TABLE `funktion` DISABLE KEYS */;
INSERT INTO `funktion` VALUES (2,'Chefexperte'),(3,'Ersatzexperte'),(1,'Experte');
/*!40000 ALTER TABLE `funktion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `kandidat`
--

DROP TABLE IF EXISTS `kandidat`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `kandidat` (
  `pid` int(11) NOT NULL DEFAULT '0',
  `kandnr` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`pid`),
  UNIQUE KEY `personkandidat` (`pid`),
  KEY `pid` (`pid`),
  CONSTRAINT `FK_person_kandidat` FOREIGN KEY (`pid`) REFERENCES `person` (`pid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `kandidat`
--

LOCK TABLES `kandidat` WRITE;
/*!40000 ALTER TABLE `kandidat` DISABLE KEYS */;
INSERT INTO `kandidat` VALUES (4,303),(5,306),(7,309),(9,310),(10,311);
/*!40000 ALTER TABLE `kandidat` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `person`
--

DROP TABLE IF EXISTS `person`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `person` (
  `pid` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `vorname` varchar(50) DEFAULT NULL,
  `tel_privat` varchar(20) DEFAULT NULL,
  `tel_geschaeft` varchar(20) DEFAULT NULL,
  `tel_mobil` varchar(20) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `adr_strasse` varchar(50) DEFAULT NULL,
  `adr_plz` varchar(6) DEFAULT NULL,
  `adr_ort` varchar(50) DEFAULT NULL,
  `firma` int(11) DEFAULT '0',
  PRIMARY KEY (`pid`),
  KEY `firmaperson` (`firma`),
  KEY `pid` (`pid`),
  CONSTRAINT `FK_firma_person` FOREIGN KEY (`firma`) REFERENCES `firma` (`fid`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `person`
--

LOCK TABLES `person` WRITE;
/*!40000 ALTER TABLE `person` DISABLE KEYS */;
INSERT INTO `person` VALUES (4,'Müller','Ingrid','032 623 48 27','','','','Alte-Bernstrasse 18','4500','Solothurn',100),(5,'Sommer','Urs','032 623 38 60','','','','Baselstr. 86','4500','Solothurn',102),(6,'Smercnik','Robert','062 296 48 48','','','','Reiserstrasse 118','4600','Olten',101),(7,'Meier','Kurt','062 212 39 02','','','','Schänggelistrasse 38','4600','Olten',100),(8,'Hufschmid','Sascha','062 212 10 43','','','','Rumpelweg 2','4612','Wangen',101),(9,'Rohner','Fabrice','062 296 70 14','','','','Paul-Brandt-Strasse 12','4600','Olten',103),(10,'Camenzind','Petra','062 296 70 78','','','','Krummackerweg 33','4600','Olten',100);
/*!40000 ALTER TABLE `person` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `spezialgebiet`
--

DROP TABLE IF EXISTS `spezialgebiet`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `spezialgebiet` (
  `sid` int(11) NOT NULL AUTO_INCREMENT,
  `bez` varchar(50) NOT NULL,
  PRIMARY KEY (`sid`),
  KEY `sid` (`sid`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `spezialgebiet`
--

LOCK TABLES `spezialgebiet` WRITE;
/*!40000 ALTER TABLE `spezialgebiet` DISABLE KEYS */;
INSERT INTO `spezialgebiet` VALUES (1,'Netzwerktechnik'),(2,'Datenbanktechnik'),(3,'Systemtechnik');
/*!40000 ALTER TABLE `spezialgebiet` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wissen`
--

DROP TABLE IF EXISTS `wissen`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wissen` (
  `pid` int(11) NOT NULL DEFAULT '0',
  `sid` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`pid`,`sid`),
  KEY `expertewissen` (`pid`),
  KEY `pid` (`pid`),
  KEY `sid` (`sid`),
  KEY `spezialgebietwissen` (`sid`),
  CONSTRAINT `FK_spezialgebiet_wissen` FOREIGN KEY (`sid`) REFERENCES `spezialgebiet` (`sid`),
  CONSTRAINT `FK_experte_wissen` FOREIGN KEY (`pid`) REFERENCES `experte` (`pid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wissen`
--

LOCK TABLES `wissen` WRITE;
/*!40000 ALTER TABLE `wissen` DISABLE KEYS */;
INSERT INTO `wissen` VALUES (6,1),(8,3);
/*!40000 ALTER TABLE `wissen` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `zuteilung`
--

DROP TABLE IF EXISTS `zuteilung`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `zuteilung` (
  `pid` int(11) NOT NULL DEFAULT '0',
  `aid` int(11) NOT NULL DEFAULT '0',
  `wunsch` datetime DEFAULT NULL,
  `definitiv` datetime DEFAULT NULL,
  PRIMARY KEY (`pid`,`aid`),
  KEY `aid` (`aid`),
  KEY `aufgabenstellungzuteilung` (`aid`),
  KEY `expertezuteilung` (`pid`),
  KEY `pid` (`pid`),
  CONSTRAINT `FK_experte_zuteilung` FOREIGN KEY (`pid`) REFERENCES `experte` (`pid`),
  CONSTRAINT `FK_aufgabenstellung_zuteilung` FOREIGN KEY (`aid`) REFERENCES `aufgabenstellung` (`aid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `zuteilung`
--

LOCK TABLES `zuteilung` WRITE;
/*!40000 ALTER TABLE `zuteilung` DISABLE KEYS */;
INSERT INTO `zuteilung` VALUES (6,4,'2007-06-19 00:00:00','0000-00-00 00:00:00'),(8,7,'2007-06-21 00:00:00','2007-06-30 00:00:00');
/*!40000 ALTER TABLE `zuteilung` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-04-28 14:28:02
