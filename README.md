# Views task
> Solution by Christof Weickhardt

## Database data & model

As reverse engineered the model looks like the picture shown bellow. You can find more information in the `database` folder which includes a ddl with some basic data, the picture bellow as png and pdf as well as the mysql workbench file
![Database model](./database/model.png)

## Basic queries

Each query is in its separate file in the `queries` folder

## Views

To create the views use the files supplied in the `views` folder. If you have a view called `v_experte`, `v_kandidat` or `v_arbeiten` it is recommended to clear or drop them in order to use the supplied ones

### v_experte
```
CREATE VIEW v_experte AS SELECT name as Namen, vorname as Vornamen, tel_privat as `Private Telefonnummer` FROM experte LEFT JOIN person ON experte.pid = person.pid ORDER BY Name, Vorname;
```

### v_kandidat
```
CREATE VIEW v_kandidat AS SELECT KandNr, person.Vorname as Vorname, person.Name as Name, firma.Name as Firmenname FROM kandidat LEFT JOIN person ON kandidat.pid = person.pid LEFT JOIN firma ON person.firma = firma.fid ORDER BY KandNr;
```

### v_arbeiten
```
CREATE VIEW v_arbeiten AS SELECT KandNr as Kandidatennummer, person.name AS KandidatName, person.vorname AS KandidatVorname, aufgabenstellung.voll AS Aufgabenstellung, expertUser.Name AS ExpertenName, spezialgebiet.bez AS Spezialgebiet FROM zuteilung
LEFT JOIN experte ON zuteilung.pid = experte.pid
LEFT JOIN person AS expertUser ON experte.pid = expertUser.pid
LEFT JOIN aufgabenstellung ON zuteilung.aid = aufgabenstellung.aid
LEFT JOIN kandidat ON aufgabenstellung.aid = kandidat.pid
LEFT JOIN person ON kandidat.pid = person.pid
LEFT JOIN spezialgebiet ON aufgabenstellung.spezialgebiet = spezialgebiet.sid
ORDER BY Kandidatennummer;
```

## Commands

### DROP VIEW
`DROP VIEW [view_name]` drops the view with the supplied view name
### ALTER VIEW
`ALTER VIEW [view_name]` is used to change the algorithmic behind a view or just its select statement
### SHOW CRATE VIEW
`SHOW CRATE VIEW [view_name]` shows the definition of the view
